/* Predator-prey simulation. Foxes and mice.
 *
 * Version 3.0, last updated in September 2023.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdbool.h>

void get_version(int* major, int* minor)
{
    *major = 3;
    *minor = 0;
}

void simulate(double r, double a, double k, double b, double m, double l, double dt, int t, int d, char* lfile, int mseed, int fseed)
{
    int version_major;
    int version_minor;
    get_version(&version_major, &version_minor);
    printf("Predator-prey simulation version %d.%d\n", version_major, version_minor);

    int w;
    int h;
    int wh;
    int hh;
    FILE* F = fopen(lfile, "r");
    if(F)
    {
        fscanf(F, "%d %d\n", &w, &h);
        printf("Width: %d. Height: %d\n", w, h);
        wh = w + 2; // Width including halo
        hh = h + 2; // Height including halo
        int lscape[hh][wh];
        for(int i = 0; i < hh; i++)
        {
            for(int j = 0; j < wh; j++)
            {
                lscape[i][j] = 0;
            }
        }

        for(int row = 1; row <= h; row++)
        {
            lscape[row][0] = 0;
            lscape[row][w-1] = 0;
            for(int i = 1; i <= w; i++)
            {
                fscanf(F, "%d", &lscape[row][i]);
            }
            fscanf(F, "\n");
        }

        int nlands = 0;
        for(int i = 1; i <= h; i++)
        {
            for(int j = 1; j <= w; j++)
            {
                if(lscape[i][j] != 0)
                {
                    nlands++;
                }
            }
        }
        printf("Number of land-only squares: %d.\n", nlands);

        // Pre-calculate number of land neighbours of each land square.
        int neibs[hh][wh];
        for(int i = 1; i <= h; i++)
        {
            for(int j = 1; j <= w; j++)
            {
                neibs[i][j] = 0;
                neibs[i][j] = lscape[i-1][j]
                            + lscape[i+1][j]
                            + lscape[i][j-1]
                            + lscape[i][j+1];
            }
        }

        double ms[hh][wh];
        double fs[hh][wh];
        for(int i = 0; i < hh; i++)
        {
            for(int j = 0; j < wh; j++)
            {
                ms[i][j] = (double)lscape[i][j];
                fs[i][j] = (double)lscape[i][j];
            }
        }
        srand(mseed);
        for(int x = 1; x < h+1; x++)
        {
            for(int y = 1; y < w+1; y++)
            {
                if(mseed==0)
                {
                    ms[x][y] = 0;
                }
                else
                {
                    if(lscape[x][y] != 0)
                    {
                        ms[x][y] = ((double)rand()) / ((double)RAND_MAX) * 5.0;
                    }
                    else
                    {
                        ms[x][y] = 0;
                    }
                }
            }
        }
        srand(fseed);
        for(int x = 1; x < h+1; x++)
        {
            for(int y = 1; y < w+1; y++)
            {
                if(fseed==0)
                {
                    fs[x][y] = 0;
                }
                else
                {
                    if(lscape[x][y] != 0)
                    {
                        fs[x][y] = ((double)rand()) / ((double)RAND_MAX) * 5.0;
                    }
                    else
                    {
                        fs[x][y] = 0;
                    }
                }
            }
        }
        // Create copies of initial maps and arrays for PPM file maps.
        // Reuse these so we don't need to create new arrays going
        // round the simulation loop.
        double ms_nu[hh][wh];
        for(int i = 0; i < hh; i++)
        {
            for(int j = 0; j < wh; j++)
            {
                ms_nu[i][j] = ms[i][j];
            }
        }
        double fs_nu[hh][wh];
        for(int i = 0; i < hh; i++)
        {
            for(int j = 0; j < wh; j++)
            {
                fs_nu[i][j] = fs[i][j];
            }
        }
        int mcols[h][w];
        for(int i = 0; i < h; i++)
        {
            for(int j = 0; j < w; j++)
            {
                mcols[i][j] = 0;
            }
        }
        int fcols[h][w];
        for(int i = 0; i < h; i++)
        {
            for(int j = 0; j < w; j++)
            {
                fcols[i][j] = 0;
            }
        }
        double am;
        double af;
        if(nlands != 0)
        {
            double total = 0.0;
            for(int i = 0; i < hh; i++)
            {
                for(int j = 0; j < wh; j++)
                {
                    total += ms[i][j];
                }
            }
            total /= nlands;
            am = total;
            total = 0.0;
            for(int i = 0; i < hh; i++)
            {
                for(int j = 0; j < wh; j++)
                {
                    total += fs[i][j];
                }
            }
            total /= nlands;
            af = total;
        }
        else
        {
            am = 0;
            af = 0;
        }
        printf("Averages. Timestep: 0 Time (s): 0 Mice: %.17f Foxes: %.17f\n", am, af);
        FILE* f2 = fopen("averages.csv","w");
        if(f2)
        {
            char hdr[] = "Timestep,Time,Mice,Foxes\n";
            fprintf(f2, "%s", hdr);
            fclose(f2);
        }
        else
        {
            exit(-1);
        }
        int tot_ts = (int)(d / dt);
        for(int i = 0; i < tot_ts; i++)
        {
            if(!(i%t))
            {
                double mm = ms[0][0];
                for(int i = 0; i < hh; i++)
                {
                    for(int j = 0; j < wh; j++)
                    {
                        if(mm < ms[i][j])
                        {
                            mm = ms[i][j];
                        }
                    }
                }
                double mf = fs[0][0];
                for(int i = 0; i < hh; i++)
                {
                    for(int j = 0; j < wh; j++)
                    {
                        if(mf < fs[i][j])
                        {
                            mf = fs[i][j];
                        }
                    }
                }
                if(nlands != 0)
                {
                    double total = 0.0;
                    for(int i = 0; i < hh; i++)
                    {
                        for(int j = 0; j < wh; j++)
                        {
                            total += ms[i][j];
                        }
                    }
                    total /= (double)nlands;
                    am = total;
                    total = 0.0;
                    for(int i = 0; i < hh; i++)
                    {
                        for(int j = 0; j < wh; j++)
                        {
                            total += fs[i][j];
                        }
                    }
                    total /= (double)nlands;
                    af = total;
                }
                else
                {
                    am = 0;
                    af = 0;
                }
                printf("Averages. Timestep: %d Time (s): %.1f Deer: %.17f Wolves: %.17f\n", i, i*dt, am, af);
                f2 = fopen("averages.csv", "a");
                if(f2)
                {
                    fprintf(f2, "%d,%.1f,%.17f,%.17f\n", i, i*dt, am, af);
                    fclose(f2);
                }
                else
                {
                    exit(-1);
                }
                int mcol;
                int fcol;
                for(int x = 1; x < h+1; x++)
                {
                    for(int y = 1; y < w+1; y++)
                    {
                        if(lscape[x][y])
                        {
                            if(mm != 0)
                            {
                                mcol = (ms[x][y]/mm) * 255;
                            }
                            else
                            {
                                mcol = 0;
                            }
                            if(mf != 0)
                            {
                                fcol=(fs[x][y]/mf)*255;
                            }
                            else
                            {
                                fcol = 0;
                            }
                            mcols[x-1][y-1] = mcol;
                            fcols[x-1][y-1] = fcol;
                        }
                    }
                }
                char name[256];
                snprintf(name, 256, "map_%d.ppm", i);
                f2 = fopen(name, "w");
                char hdr[256];
                snprintf(hdr, 256, "P3\n%d %d\n%d\n", w, h, 255);
                fwrite(hdr, sizeof(char), strlen(hdr), f2);
                for(int x = 0; x < h; x++)
                {
                    for(int y = 0; y < w; y++)
                    {
                        if(lscape[x+1][y+1])
                        {
                            char line[256];
                            snprintf(line, 256, "%d %d %d\n", fcols[x][y], mcols[x][y], 0);
                            fwrite(line, sizeof(char), strlen(line), f2);
                        }
                        else
                        {
                            char line[256];
                            snprintf(line, 256, "%d %d %d\n", 0, 200, 255);
                            fwrite(line, sizeof(char), strlen(line), f2);
                        }
                    }
                }
            }
            for(int x = 1; x < h+1; x++)
            {
                for(int y = 1; y < w+1; y++)
                {
                    if(lscape[x][y])
                    {
                        ms_nu[x][y]=ms[x][y]+dt*((r*ms[x][y])-(a*ms[x][y]*fs[x][y])+k*((ms[x-1][y]+ms[x+1][y]+ms[x][y-1]+ms[x][y+1])-(neibs[x][y]*ms[x][y])));
                        if(ms_nu[x][y]<0)
                        {
                            ms_nu[x][y]=0;
                        }
                        fs_nu[x][y]=fs[x][y]+dt*((b*ms[x][y]*fs[x][y])-(m*fs[x][y])+l*((fs[x-1][y]+fs[x+1][y]+fs[x][y-1]+fs[x][y+1])-(neibs[x][y]*fs[x][y])));
                        if(fs_nu[x][y]<0)
                        {
                            fs_nu[x][y]=0;
                        }
                    }
                }
            }
            // Swap arrays for next iteration.
            double tmp[hh][wh];
            for(int i = 0; i < hh; i++)
            {
                for(int j = 0; j < wh; j++)
                {
                    tmp[i][j] = ms[i][j];
                    ms[i][j] = ms_nu[i][j];
                    ms_nu[i][j] = tmp[i][j];
                }
            }
            for(int i = 0; i < hh; i++)
            {
                for(int j = 0; j < wh; j++)
                {
                    tmp[i][j] = fs[i][j];
                    fs[i][j] = fs_nu[i][j];
                    fs_nu[i][j] = tmp[i][j];
                }
            }
        }
    }
    else
    {
        exit(-1);
    }
}

void simCommLineIntf(int argc, char* argv[])
{
    double r = 0.1;
    double a = 0.05;
    double k = 0.2;
    double b = 0.03;
    double m = 0.09;
    double l = 0.2;
    double dt = 0.5;
    int t = 10;
    int d = 500;
    char lfile[256];
    bool lfile_provided = false;
    int mseed = 1;
    int fseed = 1;

    for(int i = 1; i < argc; i++)
    {
        if(strcmp(argv[i], "-r") == 0 || strcmp(argv[i], "--birth-mice") == 0)
        {
            i++;
            r = atof(argv[i]);
        }
        else if(strcmp(argv[i], "-a") == 0 || strcmp(argv[i], "--death-mice") == 0)
        {
            i++;
            a = atof(argv[i]);
        }
        else if(strcmp(argv[i], "-k") == 0 || strcmp(argv[i], "--diffusion-mice") == 0)
        {
            i++;
            k = atof(argv[i]);
        }
        else if(strcmp(argv[i], "-b") == 0 || strcmp(argv[i], "--birth-foxes") == 0)
        {
            i++;
            b = atof(argv[i]);
        }
        else if(strcmp(argv[i], "-m") == 0 || strcmp(argv[i], "--death-foxes") == 0)
        {
            i++;
            m = atof(argv[i]);
        }
        else if(strcmp(argv[i], "-l") == 0 || strcmp(argv[i], "--diffusion-foxes") == 0)
        {
            i++;
            l = atof(argv[i]);
        }
        else if(strcmp(argv[i], "-dt") == 0 || strcmp(argv[i], "--delta-t") == 0)
        {
            i++;
            dt = atof(argv[i]);
        }
        else if(strcmp(argv[i], "-t") == 0 || strcmp(argv[i], "--time_step") == 0)
        {
            i++;
            t = atoi(argv[i]);
        }
        else if(strcmp(argv[i], "-d") == 0 || strcmp(argv[i], "--duration") == 0)
        {
            i++;
            d = atoi(argv[i]);
        }
        else if(strcmp(argv[i], "-f") == 0 || strcmp(argv[i], "--landscape-file") == 0)
        {
            i++;
            if(strlen(argv[i]) >= sizeof(lfile))
            {
                printf("Size of file name is too long!\n");
                exit(-1);
            }
            strncpy(lfile, argv[i], sizeof(lfile));
            lfile_provided = true;
        }
        else if(strcmp(argv[i], "-ms") == 0 || strcmp(argv[i], "--mouse-seed") == 0)
        {
            i++;
            mseed = atoi(argv[i]);
        }
        else if(strcmp(argv[i], "-fs") == 0 || strcmp(argv[i], "--fox-seed") == 0)
        {
            i++;
            fseed = atoi(argv[i]);
        }
        else if(strcmp(argv[i], "-h") == 0 || strcmp(argv[i], "--help") == 0)
        {
            printf("usage: simulate_predator_prey [-h] [-r BIRTH_MICE] [-a DEATH_MICE] [-k DIFFUSION_MICE] [-b BIRTH_FOXES] [-m DEATH_FOXES] [-l DIFFUSION_FOXES] [-dt DELTA_T] [-t TIME_STEP] [-d DURATION] -f LANDSCAPE_FILE [-ds MOUSE_SEED] [-ws FOX_SEED]\n\n");
            printf("options:\n");
            printf("  -h, --help            show this help message and exit\n");
            printf("  -r BIRTH_MICE, --birth-mice BIRTH_MICE\n");
            printf("                        Birth rate of mice\n");
            printf("  -a DEATH_MICE, --death-mice DEATH_MICE\n");
            printf("                        Rate at which foxes eat mice\n");
            printf("  -k DIFFUSION_MICE, --diffusion-mice DIFFUSION_MICE\n");
            printf("                        Diffusion rate of mice\n");
            printf("  -b BIRTH_FOXES, --birth-foxes BIRTH_FOXES\n");
            printf("                        Birth rate of foxes\n");
            printf("  -m DEATH_FOXES, --death-foxes DEATH_FOXES\n");
            printf("                        Rate at which foxes starve\n");
            printf("  -l DIFFUSION_FOXES, --diffusion-foxes DIFFUSION_FOXES\n");
            printf("                        Diffusion rate of foxes\n");
            printf("  -dt DELTA_T, --delta-t DELTA_T\n");
            printf("                        Time step size\n");
            printf("  -t TIME_STEP, --time_step TIME_STEP\n");
            printf("                        Number of time steps at which to output files\n");
            printf("  -d DURATION, --duration DURATION\n");
            printf("                        Time to run the simulation (in timesteps)\n");
            printf("  -f LANDSCAPE_FILE, --landscape-file LANDSCAPE_FILE\n");
            printf("                        Input landscape file\n");
            printf("  -ms MOUSE_SEED, --mouse-seed MOUSE_SEED\n");
            printf("                        Random seed for initialising mouse densities\n");
            printf("  -fs FOX_SEED, --fox-seed FOX_SEED\n");
            printf("                        Random seed for initialising fox densities\n");
            exit(EXIT_SUCCESS);
        }
    }

    if(!lfile_provided)
    {
        printf("usage: simulate_predator_prey [-h] [-r BIRTH_MICE] [-a DEATH_MICE] [-k DIFFUSION_MICE] [-b BIRTH_FOXES] [-m DEATH_FOXES] [-l DIFFUSION_FOXES] [-dt DELTA_T] [-t TIME_STEP] [-d DURATION] -f LANDSCAPE_FILE [-ds MOUSE_SEED] [-ws FOX_SEED]\n\n");
        printf("simulate_predator_prey: error: the following arguments are required: -f/--landscape-file");
        exit(EXIT_FAILURE);
    }

    simulate(r, a, k, b, m, l, dt, t, d, lfile, mseed, fseed);
}

#ifndef TESTING
int main(int argc, char* argv[])
{
    simCommLineIntf(argc, argv);

    return EXIT_SUCCESS;
}
#endif
