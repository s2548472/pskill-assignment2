Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  Ts/call  Ts/call  name    
  7.81      0.03     0.03                             simulate (main.c:359 @ 403159)
  6.25      0.05     0.02                             simulate (main.c:340 @ 403011)
  6.25      0.07     0.02                             simulate (main.c:367 @ 4032a9)
  6.25      0.09     0.02                             simulate (main.c:365 @ 4032ea)
  6.25      0.11     0.02                             simulate (main.c:318 @ 403c02)
  6.25      0.13     0.02                             simulate (main.c:328 @ 403ca8)
  4.69      0.14     0.02                             simulate (main.c:344 @ 403004)
  4.69      0.16     0.02                             simulate (main.c:344 @ 403021)
  4.69      0.17     0.02                             simulate (main.c:358 @ 403154)
  3.13      0.18     0.01                             simulate (main.c:223 @ 402e03)
  3.13      0.19     0.01                             simulate (main.c:339 @ 402fb3)
  3.13      0.20     0.01                             simulate (main.c:345 @ 40302f)
  3.13      0.21     0.01                             simulate (main.c:356 @ 403116)
  3.13      0.22     0.01                             simulate (main.c:359 @ 40312a)
  3.13      0.23     0.01                             simulate (main.c:358 @ 40314a)
  3.13      0.24     0.01                             simulate (main.c:360 @ 40315e)
  3.13      0.25     0.01                             simulate (main.c:365 @ 403224)
  3.13      0.26     0.01                             simulate (main.c:367 @ 4032a1)
  3.13      0.27     0.01                             simulate (main.c:368 @ 4032a5)
  3.13      0.28     0.01                             simulate (main.c:322 @ 403c39)
  3.13      0.29     0.01                             simulate (main.c:316 @ 403cef)
  1.56      0.30     0.01                             simulate (main.c:344 @ 402fe2)
  1.56      0.30     0.01                             simulate (main.c:339 @ 402fe9)
  1.56      0.31     0.01                             simulate (main.c:339 @ 402fff)
  1.56      0.31     0.01                             simulate (main.c:342 @ 40301a)
  1.56      0.32     0.01                             simulate (main.c:287 @ 40395d)
  1.56      0.32     0.01                             simulate (main.c:289 @ 403965)
  0.00      0.32     0.00        1     0.00     0.00  simulate (main.c:18 @ 401ad0)

			Call graph


granularity: each sample hit covers 2 byte(s) for 3.12% of 0.32 seconds

index % time    self  children    called     name
                0.00    0.00       1/1           simCommLineIntf (main.c:504 @ 4018fc) [112]
[28]     0.0    0.00    0.00       1         simulate (main.c:18 @ 401ad0) [28]
-----------------------------------------------

Index by function name

  [28] simulate (main.c:18 @ 401ad0) [12] simulate (main.c:345 @ 40302f) [3] simulate (main.c:367 @ 4032a9)
  [10] simulate (main.c:223 @ 402e03) [13] simulate (main.c:356 @ 403116) [4] simulate (main.c:365 @ 4032ea)
  [11] simulate (main.c:339 @ 402fb3) [14] simulate (main.c:359 @ 40312a) [26] simulate (main.c:287 @ 40395d)
  [22] simulate (main.c:344 @ 402fe2) [15] simulate (main.c:358 @ 40314a) [27] simulate (main.c:289 @ 403965)
  [23] simulate (main.c:339 @ 402fe9) [9] simulate (main.c:358 @ 403154) [5] simulate (main.c:318 @ 403c02)
  [24] simulate (main.c:339 @ 402fff) [1] simulate (main.c:359 @ 403159) [20] simulate (main.c:322 @ 403c39)
   [7] simulate (main.c:344 @ 403004) [16] simulate (main.c:360 @ 40315e) [6] simulate (main.c:328 @ 403ca8)
   [2] simulate (main.c:340 @ 403011) [17] simulate (main.c:365 @ 403224) [21] simulate (main.c:316 @ 403cef)
  [25] simulate (main.c:342 @ 40301a) [18] simulate (main.c:367 @ 4032a1)
   [8] simulate (main.c:344 @ 403021) [19] simulate (main.c:368 @ 4032a5)
