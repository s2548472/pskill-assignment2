Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  Ts/call  Ts/call  name    
 23.26      0.56     0.56                             simulate (main.c:344 @ 402d9e)
 17.44      0.98     0.42                             simulate (main.c:339 @ 402a8d)
  7.68      1.17     0.19                             simulate (main.c:367 @ 40332a)
  7.68      1.35     0.19                             simulate (main.c:369 @ 4033f6)
  7.27      1.53     0.18                             simulate (main.c:368 @ 403390)
  6.85      1.69     0.17                             simulate (main.c:358 @ 403196)
  6.85      1.86     0.17                             simulate (main.c:359 @ 4031fc)
  6.64      2.02     0.16                             simulate (main.c:360 @ 403262)
  2.08      2.07     0.05                             simulate (main.c:337 @ 402a58)
  2.08      2.12     0.05                             simulate (main.c:335 @ 402a24)
  1.66      2.16     0.04                             simulate (main.c:297 @ 4025d7)
  1.66      2.20     0.04                             simulate (main.c:356 @ 4032c8)
  1.66      2.24     0.04                             simulate (main.c:365 @ 403310)
  1.25      2.27     0.03                             simulate (main.c:345 @ 403039)
  1.04      2.29     0.03                             simulate (main.c:365 @ 40345c)
  0.83      2.31     0.02                             simulate (main.c:340 @ 402d28)
  0.83      2.33     0.02                             simulate (main.c:356 @ 40317c)
  0.42      2.34     0.01                             simulate (main.c:285 @ 402519)
  0.42      2.35     0.01                             simulate (main.c:303 @ 402638)
  0.42      2.36     0.01                             simulate (main.c:318 @ 40281b)
  0.42      2.37     0.01                             simulate (main.c:327 @ 402952)
  0.42      2.38     0.01                             simulate (main.c:328 @ 4029a0)
  0.42      2.39     0.01                             simulate (main.c:354 @ 40314a)
  0.21      2.40     0.01                             simulate (main.c:221 @ 40202e)
  0.21      2.40     0.01                             simulate (main.c:223 @ 40205d)
  0.21      2.41     0.01                             simulate (main.c:247 @ 40223d)
  0.21      2.41     0.01                             simulate (main.c:245 @ 402284)
  0.00      2.41     0.00        1     0.00     0.00  get_version (main.c:12 @ 400b24)
  0.00      2.41     0.00        1     0.00     0.00  simCommLineIntf (main.c:381 @ 4035f3)
  0.00      2.41     0.00        1     0.00     0.00  simulate (main.c:18 @ 400b59)

			Call graph


granularity: each sample hit covers 2 byte(s) for 0.41% of 2.41 seconds

index % time    self  children    called     name
                0.00    0.00       1/1           simulate (main.c:21 @ 400bcf) [130]
[28]     0.0    0.00    0.00       1         get_version (main.c:12 @ 400b24) [28]
-----------------------------------------------
                0.00    0.00       1/1           main (main.c:510 @ 40438b) [39]
[29]     0.0    0.00    0.00       1         simCommLineIntf (main.c:381 @ 4035f3) [29]
-----------------------------------------------
                0.00    0.00       1/1           simCommLineIntf (main.c:504 @ 404305) [128]
[30]     0.0    0.00    0.00       1         simulate (main.c:18 @ 400b59) [30]
-----------------------------------------------

Index by function name

  [28] get_version (main.c:12 @ 400b24) [20] simulate (main.c:318 @ 40281b) [17] simulate (main.c:356 @ 40317c)
  [29] simCommLineIntf (main.c:381 @ 4035f3) [21] simulate (main.c:327 @ 402952) [6] simulate (main.c:358 @ 403196)
  [30] simulate (main.c:18 @ 400b59) [22] simulate (main.c:328 @ 4029a0) [7] simulate (main.c:359 @ 4031fc)
  [24] simulate (main.c:221 @ 40202e) [10] simulate (main.c:335 @ 402a24) [8] simulate (main.c:360 @ 403262)
  [25] simulate (main.c:223 @ 40205d) [9] simulate (main.c:337 @ 402a58) [12] simulate (main.c:356 @ 4032c8)
  [26] simulate (main.c:247 @ 40223d) [2] simulate (main.c:339 @ 402a8d) [13] simulate (main.c:365 @ 403310)
  [27] simulate (main.c:245 @ 402284) [16] simulate (main.c:340 @ 402d28) [3] simulate (main.c:367 @ 40332a)
  [18] simulate (main.c:285 @ 402519) [1] simulate (main.c:344 @ 402d9e) [5] simulate (main.c:368 @ 403390)
  [11] simulate (main.c:297 @ 4025d7) [14] simulate (main.c:345 @ 403039) [4] simulate (main.c:369 @ 4033f6)
  [19] simulate (main.c:303 @ 402638) [23] simulate (main.c:354 @ 40314a) [15] simulate (main.c:365 @ 40345c)
