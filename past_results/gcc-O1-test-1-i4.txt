Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  Ts/call  Ts/call  name    
100.16      0.02     0.02                             simulate (main.c:344 @ 401760)
  0.00      0.02     0.00        1     0.00     0.00  simCommLineIntf (main.c:381 @ 401f71)
  0.00      0.02     0.00        1     0.00     0.00  simulate (main.c:18 @ 400b09)

			Call graph


granularity: each sample hit covers 2 byte(s) for 49.92% of 0.02 seconds

index % time    self  children    called     name
                0.00    0.00       1/1           simCommLineIntf (main.c:501 @ 4025f7) [114]
[2]      0.0    0.00    0.00       1         simCommLineIntf (main.c:381 @ 401f71) [2]
-----------------------------------------------
                0.00    0.00       1/1           simCommLineIntf (main.c:504 @ 40256f) [110]
[3]      0.0    0.00    0.00       1         simulate (main.c:18 @ 400b09) [3]
-----------------------------------------------

Index by function name

   [2] simCommLineIntf (main.c:381 @ 401f71) [3] simulate (main.c:18 @ 400b09) [1] simulate (main.c:344 @ 401760)
