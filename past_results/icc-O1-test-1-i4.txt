Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  Ts/call  Ts/call  name    
 50.05      0.01     0.01                             simulate (main.c:339 @ 40248f)
 50.05      0.02     0.01                             simulate (main.c:344 @ 40255e)
  0.00      0.02     0.00        1     0.00     0.00  get_version (main.c:12 @ 4027e6)
  0.00      0.02     0.00        1     0.00     0.00  simCommLineIntf (main.c:381 @ 400f12)
  0.00      0.02     0.00        1     0.00     0.00  simulate (main.c:18 @ 4014c6)

			Call graph


granularity: each sample hit covers 2 byte(s) for 49.95% of 0.02 seconds

index % time    self  children    called     name
                0.00    0.00       1/1           simulate (main.c:21 @ 40153a) [119]
[3]      0.0    0.00    0.00       1         get_version (main.c:12 @ 4027e6) [3]
-----------------------------------------------
                0.00    0.00       1/1           main (main.c:509 @ 400ef6) [17]
[4]      0.0    0.00    0.00       1         simCommLineIntf (main.c:381 @ 400f12) [4]
-----------------------------------------------
                0.00    0.00       1/1           simCommLineIntf (main.c:504 @ 40145a) [114]
[5]      0.0    0.00    0.00       1         simulate (main.c:18 @ 4014c6) [5]
-----------------------------------------------

Index by function name

   [3] get_version (main.c:12 @ 4027e6) [5] simulate (main.c:18 @ 4014c6) [2] simulate (main.c:344 @ 40255e)
   [4] simCommLineIntf (main.c:381 @ 400f12) [1] simulate (main.c:339 @ 40248f)
