Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  Ts/call  Ts/call  name    
 23.53      1.25     1.25                             simulate (main.c:339 @ 4016cc)
 15.91      2.10     0.85                             simulate (main.c:344 @ 401760)
 10.17      2.64     0.54                             simulate (main.c:368 @ 401905)
  8.66      3.10     0.46                             simulate (main.c:322 @ 401624)
  6.02      3.42     0.32                             simulate (main.c:359 @ 401886)
  5.65      3.72     0.30                             simulate (main.c:367 @ 4018fa)
  4.14      3.94     0.22                             simulate (main.c:281 @ 40167f)
  4.14      4.16     0.22                             simulate (main.c:358 @ 40187a)
  3.95      4.37     0.21                             simulate (main.c:360 @ 401893)
  3.77      4.57     0.20                             simulate (main.c:328 @ 4015b8)
  1.98      4.68     0.11                             simulate (main.c:337 @ 4016c7)
  1.60      4.76     0.09                             simulate (main.c:369 @ 401911)
  1.51      4.84     0.08                             simulate (main.c:289 @ 4013ea)
  0.94      4.89     0.05                             simulate (main.c:257 @ 4013ba)
  0.75      4.93     0.04                             simulate (main.c:247 @ 401364)
  0.75      4.97     0.04                             simulate (main.c:304 @ 401415)
  0.75      5.01     0.04                             simulate (main.c:285 @ 401423)
  0.56      5.04     0.03                             simulate (main.c:225 @ 401307)
  0.56      5.07     0.03                             simulate (main.c:295 @ 401438)
  0.56      5.10     0.03                             simulate (main.c:327 @ 401590)
  0.56      5.13     0.03                             simulate (main.c:318 @ 4015f0)
  0.56      5.16     0.03                             simulate (main.c:321 @ 4015f8)
  0.38      5.18     0.02                             simulate (main.c:277 @ 4013e0)
  0.38      5.20     0.02                             simulate (main.c:297 @ 4013fe)
  0.38      5.22     0.02                             simulate (main.c:314 @ 401655)
  0.19      5.23     0.01                             simulate (main.c:223 @ 401303)
  0.19      5.24     0.01                             simulate (main.c:234 @ 40134d)
  0.19      5.25     0.01                             simulate (main.c:236 @ 401351)
  0.19      5.26     0.01                             simulate (main.c:303 @ 401410)
  0.19      5.27     0.01                             simulate (main.c:287 @ 401429)
  0.19      5.28     0.01                             simulate (main.c:287 @ 401483)
  0.19      5.29     0.01                             simulate (main.c:314 @ 401535)
  0.19      5.30     0.01                             simulate (main.c:316 @ 4015eb)
  0.19      5.31     0.01                             simulate (main.c:335 @ 4016be)
  0.09      5.32     0.01                             simulate (main.c:356 @ 40189d)
  0.09      5.32     0.01                             simulate (main.c:354 @ 4018a2)
  0.09      5.33     0.01                             simulate (main.c:365 @ 40191b)
  0.00      5.33     0.00        1     0.00     0.00  simCommLineIntf (main.c:381 @ 401f71)
  0.00      5.33     0.00        1     0.00     0.00  simulate (main.c:18 @ 400b09)

			Call graph


granularity: each sample hit covers 2 byte(s) for 0.19% of 5.33 seconds

index % time    self  children    called     name
                0.00    0.00       1/1           simCommLineIntf (main.c:501 @ 4025f7) [150]
[38]     0.0    0.00    0.00       1         simCommLineIntf (main.c:381 @ 401f71) [38]
-----------------------------------------------
                0.00    0.00       1/1           simCommLineIntf (main.c:504 @ 40256f) [146]
[39]     0.0    0.00    0.00       1         simulate (main.c:18 @ 400b09) [39]
-----------------------------------------------

Index by function name

  [38] simCommLineIntf (main.c:381 @ 401f71) [17] simulate (main.c:285 @ 401423) [34] simulate (main.c:335 @ 4016be)
  [39] simulate (main.c:18 @ 400b09) [30] simulate (main.c:287 @ 401429) [11] simulate (main.c:337 @ 4016c7)
  [26] simulate (main.c:223 @ 401303) [19] simulate (main.c:295 @ 401438) [1] simulate (main.c:339 @ 4016cc)
  [18] simulate (main.c:225 @ 401307) [31] simulate (main.c:287 @ 401483) [2] simulate (main.c:344 @ 401760)
  [27] simulate (main.c:234 @ 40134d) [32] simulate (main.c:314 @ 401535) [8] simulate (main.c:358 @ 40187a)
  [28] simulate (main.c:236 @ 401351) [20] simulate (main.c:327 @ 401590) [5] simulate (main.c:359 @ 401886)
  [15] simulate (main.c:247 @ 401364) [10] simulate (main.c:328 @ 4015b8) [9] simulate (main.c:360 @ 401893)
  [14] simulate (main.c:257 @ 4013ba) [33] simulate (main.c:316 @ 4015eb) [35] simulate (main.c:356 @ 40189d)
  [23] simulate (main.c:277 @ 4013e0) [21] simulate (main.c:318 @ 4015f0) [36] simulate (main.c:354 @ 4018a2)
  [13] simulate (main.c:289 @ 4013ea) [22] simulate (main.c:321 @ 4015f8) [6] simulate (main.c:367 @ 4018fa)
  [24] simulate (main.c:297 @ 4013fe) [4] simulate (main.c:322 @ 401624) [3] simulate (main.c:368 @ 401905)
  [29] simulate (main.c:303 @ 401410) [25] simulate (main.c:314 @ 401655) [12] simulate (main.c:369 @ 401911)
  [16] simulate (main.c:304 @ 401415) [7] simulate (main.c:281 @ 40167f) [37] simulate (main.c:365 @ 40191b)
