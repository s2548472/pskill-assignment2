Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  Ts/call  Ts/call  name    
  6.51      0.26     0.26                             simulate (main.c:368 @ 4026c8)
  5.26      0.47     0.21                             simulate (main.c:347 @ 4025b2)
  5.26      0.68     0.21                             simulate (main.c:359 @ 402672)
  5.13      0.89     0.21                             simulate (main.c:339 @ 4024d2)
  5.01      1.09     0.20                             simulate (main.c:358 @ 402676)
  4.88      1.28     0.20                             simulate (main.c:344 @ 40259e)
  4.75      1.47     0.19                             simulate (main.c:339 @ 40251e)
  4.50      1.65     0.18                             simulate (main.c:368 @ 4026d0)
  4.00      1.81     0.16                             simulate (main.c:337 @ 4024c7)
  4.00      1.97     0.16                             simulate (main.c:367 @ 4026bd)
  3.50      2.11     0.14                             simulate (main.c:359 @ 40267a)
  3.50      2.25     0.14                             simulate (main.c:360 @ 40267e)
  3.00      2.37     0.12                             simulate (main.c:345 @ 4025ad)
  2.63      2.48     0.11                             simulate (main.c:344 @ 40258b)
  2.50      2.58     0.10                             simulate (main.c:339 @ 402548)
  2.25      2.67     0.09                             simulate (main.c:344 @ 402571)
  2.13      2.75     0.09                             simulate (main.c:289 @ 402190)
  2.13      2.84     0.09                             simulate (main.c:339 @ 4024f2)
  1.75      2.91     0.07                             simulate (main.c:344 @ 40255e)
  1.75      2.98     0.07                             simulate (main.c:339 @ 40256c)
  1.63      3.04     0.07                             simulate (main.c:340 @ 402587)
  1.50      3.10     0.06                             simulate (main.c:367 @ 4026cc)
  1.50      3.16     0.06                             simulate (main.c:358 @ 40266a)
  1.25      3.21     0.05                             simulate (main.c:340 @ 4024ee)
  1.25      3.26     0.05                             simulate (main.c:345 @ 402504)
  1.25      3.31     0.05                             simulate (main.c:369 @ 4026d4)
  1.13      3.36     0.05                             simulate (main.c:327 @ 402354)
  1.00      3.40     0.04                             simulate (main.c:318 @ 40230e)
  1.00      3.44     0.04                             simulate (main.c:321 @ 402316)
  1.00      3.48     0.04                             simulate (main.c:339 @ 4024c1)
  1.00      3.52     0.04                             simulate (main.c:356 @ 402682)
  0.88      3.55     0.04                             simulate (main.c:342 @ 402598)
  0.75      3.58     0.03                             simulate (main.c:234 @ 401f09)
  0.75      3.61     0.03                             simulate (main.c:234 @ 401f25)
  0.75      3.64     0.03                             simulate (main.c:245 @ 401fb1)
  0.75      3.67     0.03                             simulate (main.c:339 @ 402509)
  0.75      3.70     0.03                             simulate (main.c:344 @ 402519)
  0.63      3.73     0.03                             simulate (main.c:223 @ 401ec6)
  0.63      3.75     0.03                             simulate (main.c:285 @ 40217d)
  0.63      3.78     0.03                             simulate (main.c:322 @ 40233a)
  0.63      3.80     0.03                             simulate (main.c:339 @ 402552)
  0.50      3.82     0.02                             simulate (main.c:247 @ 401fa0)
  0.50      3.84     0.02                             simulate (main.c:257 @ 401fd0)
  0.50      3.86     0.02                             simulate (main.c:295 @ 4021aa)
  0.50      3.88     0.02                             simulate (main.c:344 @ 402542)
  0.50      3.90     0.02                             simulate (main.c:354 @ 40268a)
  0.50      3.92     0.02                             simulate (main.c:365 @ 4026d8)
  0.38      3.94     0.02                             simulate (main.c:287 @ 402184)
  0.25      3.95     0.01                             simulate (main.c:223 @ 401edb)
  0.25      3.96     0.01                             simulate (main.c:297 @ 4021b6)
  0.25      3.97     0.01                             simulate (main.c:304 @ 4021d7)
  0.25      3.98     0.01                             simulate (main.c:283 @ 4021dc)
  0.25      3.99     0.01                             simulate (main.c:335 @ 4025b8)
  0.13      3.99     0.01                             simulate (main.c:221 @ 401ed8)
  0.13      4.00     0.01                             simulate (main.c:293 @ 4021a8)
  0.13      4.00     0.01                             simulate (main.c:344 @ 40254c)
  0.00      4.00     0.00        1     0.00     0.00  get_version (main.c:12 @ 4027e6)
  0.00      4.00     0.00        1     0.00     0.00  simCommLineIntf (main.c:381 @ 400f12)
  0.00      4.00     0.00        1     0.00     0.00  simulate (main.c:18 @ 4014c6)

			Call graph


granularity: each sample hit covers 2 byte(s) for 0.25% of 4.00 seconds

index % time    self  children    called     name
                0.00    0.00       1/1           simulate (main.c:21 @ 40153a) [173]
[57]     0.0    0.00    0.00       1         get_version (main.c:12 @ 4027e6) [57]
-----------------------------------------------
                0.00    0.00       1/1           main (main.c:509 @ 400ef6) [71]
[58]     0.0    0.00    0.00       1         simCommLineIntf (main.c:381 @ 400f12) [58]
-----------------------------------------------
                0.00    0.00       1/1           simCommLineIntf (main.c:504 @ 40145a) [168]
[59]     0.0    0.00    0.00       1         simulate (main.c:18 @ 4014c6) [59]
-----------------------------------------------

Index by function name

  [57] get_version (main.c:12 @ 4027e6) [29] simulate (main.c:321 @ 402316) [14] simulate (main.c:344 @ 40258b)
  [58] simCommLineIntf (main.c:381 @ 400f12) [40] simulate (main.c:322 @ 40233a) [32] simulate (main.c:342 @ 402598)
  [59] simulate (main.c:18 @ 4014c6) [27] simulate (main.c:327 @ 402354) [6] simulate (main.c:344 @ 40259e)
  [38] simulate (main.c:223 @ 401ec6) [30] simulate (main.c:339 @ 4024c1) [13] simulate (main.c:345 @ 4025ad)
  [54] simulate (main.c:221 @ 401ed8) [9] simulate (main.c:337 @ 4024c7) [2] simulate (main.c:347 @ 4025b2)
  [49] simulate (main.c:223 @ 401edb) [4] simulate (main.c:339 @ 4024d2) [53] simulate (main.c:335 @ 4025b8)
  [33] simulate (main.c:234 @ 401f09) [24] simulate (main.c:340 @ 4024ee) [23] simulate (main.c:358 @ 40266a)
  [34] simulate (main.c:234 @ 401f25) [18] simulate (main.c:339 @ 4024f2) [3] simulate (main.c:359 @ 402672)
  [42] simulate (main.c:247 @ 401fa0) [25] simulate (main.c:345 @ 402504) [5] simulate (main.c:358 @ 402676)
  [35] simulate (main.c:245 @ 401fb1) [36] simulate (main.c:339 @ 402509) [11] simulate (main.c:359 @ 40267a)
  [43] simulate (main.c:257 @ 401fd0) [37] simulate (main.c:344 @ 402519) [12] simulate (main.c:360 @ 40267e)
  [39] simulate (main.c:285 @ 40217d) [7] simulate (main.c:339 @ 40251e) [31] simulate (main.c:356 @ 402682)
  [48] simulate (main.c:287 @ 402184) [45] simulate (main.c:344 @ 402542) [46] simulate (main.c:354 @ 40268a)
  [17] simulate (main.c:289 @ 402190) [15] simulate (main.c:339 @ 402548) [10] simulate (main.c:367 @ 4026bd)
  [55] simulate (main.c:293 @ 4021a8) [56] simulate (main.c:344 @ 40254c) [1] simulate (main.c:368 @ 4026c8)
  [44] simulate (main.c:295 @ 4021aa) [41] simulate (main.c:339 @ 402552) [22] simulate (main.c:367 @ 4026cc)
  [50] simulate (main.c:297 @ 4021b6) [19] simulate (main.c:344 @ 40255e) [8] simulate (main.c:368 @ 4026d0)
  [51] simulate (main.c:304 @ 4021d7) [20] simulate (main.c:339 @ 40256c) [26] simulate (main.c:369 @ 4026d4)
  [52] simulate (main.c:283 @ 4021dc) [16] simulate (main.c:344 @ 402571) [47] simulate (main.c:365 @ 4026d8)
  [28] simulate (main.c:318 @ 40230e) [21] simulate (main.c:340 @ 402587)
