Flat profile:

Each sample counts as 0.01 seconds.
  %   cumulative   self              self     total           
 time   seconds   seconds    calls  Ts/call  Ts/call  name    
  8.40      0.33     0.33                             simulate (main.c:345 @ 40302f)
  6.87      0.60     0.27                             simulate (main.c:339 @ 402f72)
  5.60      0.82     0.22                             simulate (main.c:358 @ 403154)
  5.47      1.04     0.22                             simulate (main.c:368 @ 4032a5)
  5.47      1.25     0.22                             simulate (main.c:367 @ 4032a9)
  5.34      1.46     0.21                             simulate (main.c:359 @ 403159)
  4.96      1.66     0.20                             simulate (main.c:344 @ 403021)
  4.58      1.84     0.18                             simulate (main.c:359 @ 40314f)
  4.33      2.01     0.17                             simulate (main.c:322 @ 403c39)
  4.07      2.17     0.16                             simulate (main.c:368 @ 4032ae)
  3.31      2.30     0.13                             simulate (main.c:337 @ 402f67)
  2.80      2.41     0.11                             simulate (main.c:347 @ 403036)
  2.80      2.52     0.11                             simulate (main.c:335 @ 40303c)
  2.80      2.63     0.11                             simulate (main.c:340 @ 403011)
  2.54      2.73     0.10                             simulate (main.c:339 @ 402fb3)
  2.54      2.83     0.10                             simulate (main.c:358 @ 40314a)
  2.16      2.91     0.09                             simulate (main.c:339 @ 402fe9)
  1.78      2.98     0.07                             simulate (main.c:344 @ 402ffa)
  1.53      3.04     0.06                             simulate (main.c:339 @ 402fd8)
  1.53      3.10     0.06                             simulate (main.c:367 @ 4032a1)
  1.53      3.16     0.06                             simulate (main.c:289 @ 403965)
  1.40      3.22     0.06                             simulate (main.c:344 @ 403004)
  1.27      3.27     0.05                             simulate (main.c:339 @ 402fcd)
  1.27      3.32     0.05                             simulate (main.c:328 @ 403ca8)
  1.15      3.36     0.05                             simulate (main.c:344 @ 402fe2)
  1.02      3.40     0.04                             simulate (main.c:223 @ 402e03)
  1.02      3.44     0.04                             simulate (main.c:339 @ 402ff5)
  1.02      3.48     0.04                             simulate (main.c:360 @ 40315e)
  0.76      3.51     0.03                             simulate (main.c:369 @ 4032b2)
  0.76      3.54     0.03                             simulate (main.c:285 @ 403947)
  0.76      3.57     0.03                             simulate (main.c:304 @ 4039a3)
  0.76      3.60     0.03                             simulate (main.c:321 @ 403c15)
  0.64      3.63     0.03                             simulate (main.c:339 @ 402fff)
  0.51      3.65     0.02                             simulate (main.c:339 @ 402f63)
  0.51      3.67     0.02                             simulate (main.c:344 @ 402fae)
  0.51      3.69     0.02                             simulate (main.c:339 @ 402fc2)
  0.51      3.71     0.02                             simulate (main.c:344 @ 402fd2)
  0.51      3.73     0.02                             simulate (main.c:356 @ 40310f)
  0.51      3.75     0.02                             simulate (main.c:287 @ 40395d)
  0.51      3.77     0.02                             simulate (main.c:318 @ 403c02)
  0.25      3.78     0.01                             simulate (main.c:223 @ 402e1f)
  0.25      3.79     0.01                             simulate (main.c:223 @ 402e47)
  0.25      3.80     0.01                             simulate (main.c:333 @ 402e7f)
  0.25      3.81     0.01                             simulate (main.c:339 @ 402f28)
  0.25      3.82     0.01                             simulate (main.c:340 @ 402fbe)
  0.25      3.83     0.01                             simulate (main.c:344 @ 402fee)
  0.25      3.84     0.01                             simulate (main.c:344 @ 403016)
  0.25      3.85     0.01                             simulate (main.c:356 @ 403116)
  0.25      3.86     0.01                             simulate (main.c:365 @ 4032b6)
  0.25      3.87     0.01                             simulate (main.c:234 @ 4035f8)
  0.25      3.88     0.01                             simulate (main.c:234 @ 40361b)
  0.25      3.89     0.01                             simulate (main.c:289 @ 4039b7)
  0.25      3.90     0.01                             simulate (main.c:297 @ 4039d8)
  0.25      3.91     0.01                             simulate (main.c:304 @ 4039f6)
  0.25      3.92     0.01                             simulate (main.c:327 @ 403c82)
  0.25      3.93     0.01                             simulate (main.c:257 @ 403e2d)
  0.13      3.93     0.01                             simulate (main.c:342 @ 40301a)
  0.00      3.93     0.00        1     0.00     0.00  simulate (main.c:18 @ 401ad0)

			Call graph


granularity: each sample hit covers 2 byte(s) for 0.25% of 3.93 seconds

index % time    self  children    called     name
                0.00    0.00       1/1           simCommLineIntf (main.c:504 @ 4018fc) [142]
[58]     0.0    0.00    0.00       1         simulate (main.c:18 @ 401ad0) [58]
-----------------------------------------------

Index by function name

  [58] simulate (main.c:18 @ 401ad0) [18] simulate (main.c:344 @ 402ffa) [10] simulate (main.c:368 @ 4032ae)
  [26] simulate (main.c:223 @ 402e03) [33] simulate (main.c:339 @ 402fff) [29] simulate (main.c:369 @ 4032b2)
  [41] simulate (main.c:223 @ 402e1f) [22] simulate (main.c:344 @ 403004) [49] simulate (main.c:365 @ 4032b6)
  [42] simulate (main.c:223 @ 402e47) [14] simulate (main.c:340 @ 403011) [50] simulate (main.c:234 @ 4035f8)
  [43] simulate (main.c:333 @ 402e7f) [47] simulate (main.c:344 @ 403016) [51] simulate (main.c:234 @ 40361b)
  [44] simulate (main.c:339 @ 402f28) [57] simulate (main.c:342 @ 40301a) [30] simulate (main.c:285 @ 403947)
  [34] simulate (main.c:339 @ 402f63) [7] simulate (main.c:344 @ 403021) [39] simulate (main.c:287 @ 40395d)
  [11] simulate (main.c:337 @ 402f67) [1] simulate (main.c:345 @ 40302f) [21] simulate (main.c:289 @ 403965)
   [2] simulate (main.c:339 @ 402f72) [12] simulate (main.c:347 @ 403036) [31] simulate (main.c:304 @ 4039a3)
  [35] simulate (main.c:344 @ 402fae) [13] simulate (main.c:335 @ 40303c) [52] simulate (main.c:289 @ 4039b7)
  [15] simulate (main.c:339 @ 402fb3) [38] simulate (main.c:356 @ 40310f) [53] simulate (main.c:297 @ 4039d8)
  [45] simulate (main.c:340 @ 402fbe) [48] simulate (main.c:356 @ 403116) [54] simulate (main.c:304 @ 4039f6)
  [36] simulate (main.c:339 @ 402fc2) [16] simulate (main.c:358 @ 40314a) [40] simulate (main.c:318 @ 403c02)
  [23] simulate (main.c:339 @ 402fcd) [8] simulate (main.c:359 @ 40314f) [32] simulate (main.c:321 @ 403c15)
  [37] simulate (main.c:344 @ 402fd2) [3] simulate (main.c:358 @ 403154) [9] simulate (main.c:322 @ 403c39)
  [19] simulate (main.c:339 @ 402fd8) [6] simulate (main.c:359 @ 403159) [55] simulate (main.c:327 @ 403c82)
  [25] simulate (main.c:344 @ 402fe2) [28] simulate (main.c:360 @ 40315e) [24] simulate (main.c:328 @ 403ca8)
  [17] simulate (main.c:339 @ 402fe9) [20] simulate (main.c:367 @ 4032a1) [56] simulate (main.c:257 @ 403e2d)
  [46] simulate (main.c:344 @ 402fee) [4] simulate (main.c:368 @ 4032a5)
  [27] simulate (main.c:339 @ 402ff5) [5] simulate (main.c:367 @ 4032a9)
