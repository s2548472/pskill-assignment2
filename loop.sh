OPTIMIZATION="O2"
sed -i 's/O[[:digit:]]/'$OPTIMIZATION'/g' Makefile
for ITERATION in i0 i1 i2 i3 i4
do
	sed -i 's/i[[:digit:]]/'$ITERATION'/g' Makefile
	make icc-$OPTIMIZATION-test-1-$ITERATION.txt
done
