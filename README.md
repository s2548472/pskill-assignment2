# MSc Programming Skills C predator-prey simulation

## Requirements

* gcc
* cmocka (https://cmocka.org)

To get gcc on Cirrus, run:

```console
$ module load gcc
```

To get cmocka, install it from a package manager (e.g. `apt install libcmocka-dev`) or download and unpack the [latest release](https://cmocka.org/files/1.1/cmocka-1.1.7.tar.xz).

---

## Usage

First, to compile the code:

```console
$ make bin/simulate_predator_prey
```

To see help:

```console
$ bin/simulate_predator_prey -h
```

To run the simulation:

```console
$ bin/simulate_predator_prey \
    [-r BIRTH_MICE] [-a DEATH_MICE] \
    [-k DIFFUSION_MICE] [-b BIRTH_FOXES] \
    [-m DEATH_FOXES] [-l DIFFUSION_FOXES] \
    [-dt DELTA_T] [-t TIME_STEP] [-d DURATION] \
    -f LANDSCAPE_FILE [-ds MOUSE_SEED] \
    [-ws FOX_SEED]
```

(where `\` denotes a line continuation character)

For example, to run using map.dat with default values for the other parameters:

```console
$ bin/simulate_predator_prey -f map.dat
```

### Command-line parameters

| Flag | Parameter | Description | Default Value |
| ---- | --------- |------------ | ------------- |
| -h | --help | Show help message and exit | - |
| -r | --birth-mice | Birth rate of mice | 0.08 |
| -a | --death-mice | Rate at which foxes eat mice | 0.04 |
| -k | --diffusion-mice | Diffusion rate of mice | 0.2 |
| -b | --birth-foxes | Birth rate of foxes | 0.02 |
| -m | --death-foxes  | Rate at which foxes starve | 0.06 |
| -l | --diffusion-foxes | Diffusion rate of foxes | 0.2 |
| -dt | --delta-t | Time step size (seconds) | 0.4 |
| -t | --time_step | Number of time stews at which to output files | 10 |
| -d | --duration  | Time to run the simulation (seconds) | 500 |
| -f | --landscape-file | Input landscape file | - |
| -ms | --mouse-seed | Random seed for initialising mouse densities. If 0 then the density in each square will be 0, else each square's density will be set to a random value between 0.0 and 5.0 | 1 |
| -fs | --fox-seed | Random seed for initialising fox densities. If 0 then the density in each square will be 0, else each square's density will be set to a random value between 0.0 and 5.0 | 1 |

### Input files

Map files are expected to be plain-text files of form:

* One line giving Nx, the number of columns, and Ny, the number of rows
* Ny lines, each consisting of a sequence of Nx space-separated ones and zeros (land=1, water=0).

For example:

```
7 7
1 1 1 1 1 1 1
1 1 1 1 1 1 1
1 1 1 1 0 1 1
1 1 1 1 0 0 1
1 1 1 0 0 0 0
1 1 1 0 0 0 0
1 0 0 0 0 0 0
```

### PPM output files

"Plain PPM" image files are output every `TIME_STEP` timesteps.  These files are named `map_<NNNN>.ppm` and are a visualisation of the density of mice and foxes and water-only squares.

These files do not include the halo as the use of a halo is an implementation detail.

These files are plain-text so you can view them as you would any plain-text file e.g.:

```console
$ cat map<NNNN>.ppm
```

PPM files can be viewed graphically using ImageMagick commands as follows.

Cirrus users will need first need to run:

```console
$ module load ImageMagick
```

To view a PPM file, run:

```console
$ display map<NNNN>.ppm
```

To animate a series of PPM files:

```console
$ animate map*.ppm
```

For more information on the PPM file format, run `man ppm` or see [ppm](http://netpbm.sourceforge.net/doc/ppm.html).

### CSV averages output file

A plain-text comma-separated values file, `averages.csv`, has the average density of deer and wolves (across the land-only squares) calculated every `TIME_STEP` timesteps. The file has four columns and a header row:

```csv
Timestep,Time,Mice,Foxes
```

where:

* `Timestep`: timestep from 0 .. `DURATION` / `DELTA_T`
* `Time`: time in seconds from 0 .. `DURATION`
* `Mice`: average density of mice.
* `Foxes`: average density of foxes.

This file is plain-text so you can view it as you would any plain-text file e.g.:

```console
$ cat averages.csv
```

---

## Running automated tests

`test/test_example.c` is a module with a unit test for the `get_version` function in `predator_prey/main.c`.

You can compile this code and run it with:

```console
$ make test/test_example
$ test/test_example
1..1
ok 1 - test_get_version
# ok - tests
```
