test-1 := -d 5000 -dt 0.5 -r 0.5 -b 0.02 -f map-10.dat
test-2 := -d 10000 -dt 0.25 -r 0.56667 -b 0.01667 -f map-30.dat
test-3 := -d 13000 -dt 0.175 -r 0.523571 -b 0.01623571 -f map-60.dat
test-4 := -d 15000 -dt 0.1 -r 0.52357111317 -b 0.0162357111317 -f map-100.dat
OPT-FLAG := O2
ITE-FLAG := i4

icc-$(OPT-FLAG)-test-1-$(ITE-FLAG).txt : bin/icc_$(OPT-FLAG)_simulate_predator_prey
	bin/icc_$(OPT-FLAG)_simulate_predator_prey $(test-1)
	gprof -l -b bin/icc_$(OPT-FLAG)_simulate_predator_prey gmon.out > icc-$(OPT-FLAG)-test-1-$(ITE-FLAG).txt
	rm *.ppm

icc-$(OPT-FLAG)-test-2-$(ITE-FLAG).txt : bin/icc_$(OPT-FLAG)_simulate_predator_prey
	bin/icc_$(OPT-FLAG)_simulate_predator_prey $(test-2)
	gprof -l -b bin/icc_$(OPT-FLAG)_simulate_predator_prey gmon.out > icc-$(OPT-FLAG)-test-2-$(ITE-FLAG).txt
	rm *.ppm

icc-$(OPT-FLAG)-test-3-$(ITE-FLAG).txt : bin/icc_$(OPT-FLAG)_simulate_predator_prey
	bin/icc_$(OPT-FLAG)_simulate_predator_prey $(test-3)
	gprof -l -b bin/icc_$(OPT-FLAG)_simulate_predator_prey gmon.out > icc-$(OPT-FLAG)-test-3-$(ITE-FLAG).txt
	rm *.ppm

icc-$(OPT-FLAG)-test-4-$(ITE-FLAG).txt : bin/icc_$(OPT-FLAG)_simulate_predator_prey
	bin/icc_$(OPT-FLAG)_simulate_predator_prey $(test-4)
	gprof -l -b bin/icc_$(OPT-FLAG)_simulate_predator_prey gmon.out > icc-$(OPT-FLAG)-test-4-$(ITE-FLAG).txt
	rm *.ppm

gcc-$(OPT-FLAG)-test-1-$(ITE-FLAG).txt : bin/gcc_$(OPT-FLAG)_simulate_predator_prey
	bin/gcc_$(OPT-FLAG)_simulate_predator_prey $(test-1)
	gprof -l -b bin/gcc_$(OPT-FLAG)_simulate_predator_prey gmon.out > gcc-$(OPT-FLAG)-test-1-$(ITE-FLAG).txt
	rm *.ppm

gcc-$(OPT-FLAG)-test-2-$(ITE-FLAG).txt : bin/gcc_$(OPT-FLAG)_simulate_predator_prey
	bin/gcc_$(OPT-FLAG)_simulate_predator_prey $(test-2)
	gprof -l -b bin/gcc_$(OPT-FLAG)_simulate_predator_prey gmon.out > gcc-$(OPT-FLAG)-test-2-$(ITE-FLAG).txt
	rm *.ppm

gcc-$(OPT-FLAG)-test-3-$(ITE-FLAG).txt : bin/gcc_$(OPT-FLAG)_simulate_predator_prey
	bin/gcc_$(OPT-FLAG)_simulate_predator_prey $(test-3)
	gprof -l -b bin/gcc_$(OPT-FLAG)_simulate_predator_prey gmon.out > gcc-$(OPT-FLAG)-test-3-$(ITE-FLAG).txt
	rm *.ppm

gcc-$(OPT-FLAG)-test-4-$(ITE-FLAG).txt : bin/gcc_$(OPT-FLAG)_simulate_predator_prey
	bin/gcc_$(OPT-FLAG)_simulate_predator_prey $(test-4)
	gprof -l -b bin/gcc_$(OPT-FLAG)_simulate_predator_prey gmon.out > gcc-$(OPT-FLAG)-test-4-$(ITE-FLAG).txt
	rm *.ppm

bin/gcc_$(OPT-FLAG)_simulate_predator_prey : build/prod/gcc-$(OPT-FLAG)-main.o
	mkdir -p bin/
	gcc -o bin/gcc_$(OPT-FLAG)_simulate_predator_prey -g -pg -$(OPT-FLAG) build/prod/gcc-$(OPT-FLAG)-main.o

build/prod/gcc-$(OPT-FLAG)-main.o : predator_prey/main.c
	mkdir -p build/prod/
	gcc -c -o build/prod/gcc-$(OPT-FLAG)-main.o -g -pg -$(OPT-FLAG) predator_prey/main.c

bin/icc_$(OPT-FLAG)_simulate_predator_prey : build/prod/icc-$(OPT-FLAG)-main.o
	mkdir -p bin/
	icc -o bin/icc_$(OPT-FLAG)_simulate_predator_prey -g -pg -$(OPT-FLAG) build/prod/icc-$(OPT-FLAG)-main.o

build/prod/icc-$(OPT-FLAG)-main.o : predator_prey/main.c
	mkdir -p build/prod/
	icc -c -o build/prod/icc-$(OPT-FLAG)-main.o -g -pg -$(OPT-FLAG) predator_prey/main.c

.PHONY : clean
clean :
	rm -rf bin/ build/
	rm gmon.out
	rm *.ppm
	rm *.csv
