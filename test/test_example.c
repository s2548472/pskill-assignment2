#include <stdarg.h>
#include <stddef.h>
#include <stdint.h>
#include <setjmp.h>
#include <cmocka.h>

extern void get_version(int* major, int* minor);

/* A test case that does nothing and succeeds. */
static void test_get_version(void **state) {
    (void) state; /* unused */
    int version_major;
    int version_minor;
    get_version(&version_major, &version_minor);

    assert_int_equal(version_major, 3);
    assert_int_equal(version_minor, 0);
}

#ifdef TESTING
int main(void) {
    const struct CMUnitTest tests[] = {
        cmocka_unit_test(test_get_version),
    };
    cmocka_set_message_output(CM_OUTPUT_TAP);
    return cmocka_run_group_tests(tests, NULL, NULL);
}
#endif
